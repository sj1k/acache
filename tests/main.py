import sys
sys.path.insert(0, '.')  # :c

import time
import acache
import asyncio


def test_base():
    base = acache.strategy.Base()
    assert _try(base.filter)
    assert _try(base.set, 10, 100)
    assert _try(base.get, 100)
    return None


def _try(func, *args, **kwargs):
    try:
        func(*args, **kwargs)
    except NotImplementedError:
        return True
    return False


def test_normal():

    @acache.cache(2, strat=acache.strategy.Basic, runner=acache.runners.normal)
    def t(*args, **kwargs):
        return time.time()

    one = t(1)
    one_again = t(1)

    assert one == one_again
    assert one != t(2)
    assert one != t(3)

    assert one != t(1)  # We've called more items than cache limit, this should be false now.
    return None


def test_async():
    loop = asyncio.get_event_loop()
    loop.run_until_complete(_test_async())
    return None


async def _test_async():

    @acache.acache(2, strat=acache.strategy.Basic)
    async def t(*args, **kwargs):
        return time.time()

    one = await t(1)
    one_again = await t(1)

    assert one == one_again
    assert one != await t(2)
    assert one != await t(3)

    assert one != await t(1)  # We've called more items than cache limit, this should be false now.
    return None


def test_lru():

    @acache.cache(2, strat=acache.strategy.lru, runner=acache.runners.raw)
    def t(*args, **kargs):
        return time.time()

    one = t(1)
    one_again = t(1)

    assert one == one_again
    assert one != t(2)
    assert one != t(3)

    assert one != t(1)  # We've called more items than cache limit, this should be false now.

    return None


def test_LRU():

    @acache.cache(2, strat=acache.strategy.LRU, runner=acache.runners.normal)
    def t(*args, **kwargs):
        return time.time()

    one = t(1)
    two = t(2)
    one_again = t(1)

    assert one == one_again
    assert one != two

    t(3) # Should wipe 2, 1 was last used.
    assert two != t(2)
    assert one != t(1)


def test_timed():

    @acache.cache(1, strat=acache.strategy.Timed, runner=acache.runners.normal)
    def t(*args, **kwargs):
        return time.time()

    one = t(1)
    one_again = t(1)
    two = t(2)

    assert one == one_again
    assert one != two

    time.sleep(2)

    assert one != t(1)
    assert two != t(2)

    return None
