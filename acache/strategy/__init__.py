from .basic import Basic
from .timed import Timed

from .lru import lru, LRU
from .base import Base

from . import hash
