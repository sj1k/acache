import acache
import time

@acache.cache(100)  # Creates a cache which stores 100 items, using default strategies and runners.
def myfunc(thing):
   time.sleep(5)
   return thing

print(myfunc('Thing'))
print(myfunc('Thing'))  # This will not have any delay, using the previous value. 
