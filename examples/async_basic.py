import acache
import time
import asyncio

@acache.acache(100)  # Creates a cache which stores 100 items, using default strategies and runners.
async def myfunc(thing):
   await asyncio.sleep(5)
   return thing


async def main():
   print(await myfunc('Thing'))
   print(await myfunc('Thing'))  # This will not have any delay, using the previous value.  
   return None


loop = asyncio.get_event_loop()
loop.run_until_complete(main())
