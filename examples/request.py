import requests
import asyncio
import time

from acache import acache


@acache()
async def req(*args, **kwargs):
    return requests.get(*args, **kwargs)


async def main():
    start = time.time()
    await req('https://www.google.com')

    mid = time.time()
    await req('https://www.google.com')
    await req('https://www.google.com')

    end = time.time()
    print('First: {}\nSecond: {:.15f}'.format(mid - start, end - mid))
    return None


loop = asyncio.get_event_loop()
loop.run_until_complete(main())
